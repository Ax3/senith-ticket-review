﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Senith.Models
{
    public class Ticket
    {
        public string Destino { get; set; }
        public string HoraSalida { get; set; }
        public string Clase { get; set; }
        public Boolean Disponible { get; set; }
    }
}
