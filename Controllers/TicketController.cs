﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Senith.Seeds;
using Senith.Models;
using System.Data;
using System.Data.SqlClient;

namespace Senith.Controllers
{
    public class TicketController : Controller
    {
        // string connectionString = @"Data Source = (local)\sqle2017; Initial Catalog = SenithDB;  Integrated Security=True";

        private TicketsSeeder _ticketSeeder;

        public TicketController()
        {
            _ticketSeeder = new TicketsSeeder();
        }

        // GET: Ticket
        [HttpGet]
        public ActionResult MostrarTickets()
        {
            ViewBag.Message = "El listado de vuelo es: ";

            // DataTable dtblTicket = new DataTable();
            // using (SqlConnection sqlCon = new SqlConnection(connectionString))
            // {
                // sqlCon.Open();
                // SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM Ticket", sqlCon);
              //  sqlDa.Fill(dtblTicket);
            //}

            var model = _ticketSeeder.ObtenerTickets();

            return View(model);
        }

        [HttpGet]
        public ActionResult ReservarTicket()
        {
            ViewBag.Message = "Por favor, proceda a reservar su Ticket de vuelo: ";

            return View(new Ticket());
        }

        [HttpPost]
        public ActionResult ReservarTicket(FormatException collection)
        {
            ViewBag.Message = "Por favor, proceda a reservar su Ticket de vuelo: ";

            return View(new Ticket());

        }
    }
}