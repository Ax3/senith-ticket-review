﻿using Senith.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Senith.Seeds
{
    public class TicketsSeeder
    {
        public List<Ticket> ObtenerTickets()
        {
            return new List<Ticket>()
            {
                new Ticket()
                {
                    Destino = "Hong Kong",
                    HoraSalida = "21:00",
                    Clase = "Ejecutiva",
                    Disponible = true
                },
                new Ticket()
                {
                    Destino = "Berlin",
                    HoraSalida = "19:30",
                    Clase = "Ejecutiva",
                    Disponible = true
        },
                new Ticket()
                {
                    Destino = "Madrid",
                    HoraSalida = "17:45",
                    Clase = "Normal",
                    Disponible = true
        },
            };
        }
    }
}
